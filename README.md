# README #

An algorithm for the region interpolation problem (RIP) using morphing techniques to interpolate 2D regions in the context of spatiotemporal databases.

The algorithm is still being developed and in its current version can only handle simple polygons.

Because it as not been published yet only the binaries are available.

The algorithm is implemented using VS 2017.

### Dependencies ###

* GEOS
* Armadillo
* Blas, LPACK ...


### Using the Binaries ###

* Under construction ...

### Contacts ###

* José Duarte: IEETA, University of Aveiro, hfduarte@ua.pt
